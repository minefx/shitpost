const Discord = require("discord.js");
const botIntents = new Discord.Intents(Discord.Intents.NON_PRIVILEGED);
botIntents.add("GUILD_MEMBERS");
const client = new Discord.Client({ ws: { intents: botIntents }});
const fs = require("fs");
require("dotenv").config();
const pack = require("./package.json");
const sql = require("./libs/sqlite.js");
const defaultSettings = require("./defaultSettings.js");

client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

client.events = new Discord.Collection();
const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('js'));
eventFiles.forEach(file => {
	const eventName = file.split(".")[0];
	const event = require(`./events/${file}`)

	client.on(eventName, event.bind(null, client));
});

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);

	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
};

client.on("message", async message => {
  let settings = sql.prepare("SELECT * FROM botsettings WHERE guild = ?").get(message.guild.id);
  if (!settings) {
		settings = defaultSettings;
  }

  const prefix = settings.prefix;
  const prefixMention = new RegExp(`^<@!?${client.user.id}>( |)$`);
  if (message.content.match(prefixMention)) {
		return message.channel.send(`Bot prefix for this guild is \`${settings.prefix}\`!`);
  }
  else if (!message.content.startsWith(prefix) || message.author.bot) return;
	const args = message.content.slice(prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();

	if (!client.commands.has(commandName)) return;

  const command = client.commands.get(commandName);

	try {
		command.execute(client, message, args, settings);
	} catch (error) {
    const errorEmbed = new Discord.MessageEmbed()
      .setTitle("Error")
      .setDescription(error)
      .setTimestamp()
      .setFooter("Please report this to an administrator!");
		console.error(error);
    return message.channel.send("There was an error trying to execute that command!", errorEmbed);
	};
	return;
});

client.login(process.env.TOKEN);