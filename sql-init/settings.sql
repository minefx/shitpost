CREATE TABLE `botSettings`(
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    `guild` INTEGER NOT NULL,
    `prefix` TEXT NOT NULL,
    `cashModRole` TEXT NOT NULL,
    `botAdminRole` TEXT NOT NULL,
    `paydayTime` INTEGER NOT NULL,
    `robTime` INTEGER NOT NULL
);