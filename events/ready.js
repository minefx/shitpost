const pack = require('../package.json')
const sql = require("../libs/sqlite.js");
const fs = require('fs');
module.exports = (client) => {
    client.user.setPresence({
        status: 'streaming',
        activity: {
            name: `s!help | ${client.guilds.cache.size} servers! | Version: ${pack.version}`,
            type: 'PLAYING',
        }
    })
    const cashTable = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'cash';").get();
    if (!cashTable["count(*)"]) {
        fs.readFile("./sql-init/cash.sql", "utf8", async (err, data) => {
            if (err) throw new Error(err);
            await sql.prepare(data).run();
          });
          sql.pragma("synchronous = 1");
          sql.pragma("journal_mode = wal");
    }
    const setTable = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'botSettings';").get();
    if (!setTable["count(*)"]) {
        fs.readFile("./sql-init/settings.sql", "utf8", async (err, data) => {
            if (err) throw new Error(err);
            await sql.prepare(data).run();
          });
          sql.pragma("synchronous = 1");
          sql.pragma("journal_mode = wal");
    }
    console.log("Ready!");
}