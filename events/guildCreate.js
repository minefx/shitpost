const sql = require("../libs/sqlite");
const pack = require("../package.json");

module.exports = async (client, guild) => {
  console.log(`Joined a new server: ${guild.name} (${guild.id})`);
  await sql.prepare("INSERT INTO botsettings (guild, prefix, cashModRole, botAdminRole, paydayTime, robTime) VALUES (?, ?, ?, ?, ?, ?)")
    .run(message.guild.id, "s!", "Administrator", "Administrator", 60, 60);
    client.user.setPresence({
      status: 'streaming',
      activity: {
          name: `s!help | ${client.guilds.cache.size} servers! | Version: ${pack.version}`,
          type: 'PLAYING',
      }
  })
};