const defaultSettings = {
    "prefix": "s!",
    "cashModRole": "Administrator",
    "botAdminRole":"Administrator",
    "paydayTime": "60",
    "robTime": "60"
}

module.exports = defaultSettings;