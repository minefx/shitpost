const sql = require("../libs/sqlite.js");
const moment = require("moment")
module.exports = {
    name: "setmoney",
    description: "Set a member's money",
    async execute(client, message, args, settings) {
        const member = message.mentions.users.first();
        // If there are no arguments defined, or any checks fail, return
        if (!member) return message.reply("Who do you want to give the free money to, silly?");
        if (!args[1]) return message.reply("How much free money are you giving this person?");
        if (args[1] < 0) return message.reply("You can't give less than $0!");

        if (message.member.roles.cache.has(settings.cashModRole) || message.member.hasPermission("ADMINISTRATOR")) {
            const row = sql.prepare("SELECT * FROM cash WHERE user = ? AND guild = ?").get(member.id, message.guild.id);
            const time = parseInt(moment().add(settings.paydayTime, "seconds").unix());

            if (!row) {
                sql.prepare("INSERT INTO cash (user, cash, guild, next_payday) VALUES (?, ?, ?, ?)").run(member.id, 0, message.guild.id, time);
                return message.channel.send("That user doesn't have an account, try re-running the command again.");
            };
            console.log(row.cash)
            const newCashAmount = row.cash + parseInt(args[1]);

            await sql.prepare("UPDATE cash SET cash = ? WHERE guild = ? AND user = ?").run(args[1], message.guild.id, member.id);
            return message.reply(`:white_check_mark: Sucessfully gave ${member.tag} \$${args[1]}!`);
        } else return message.reply("You do not have permission to run that command or you have not set up the CashMGR setting! Run s!settings and set the cashmgr role!");
    },
}