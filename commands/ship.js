const Discord = require("discord.js");

module.exports = {
  name: "ship",
  description: "Ship 2 people",
  execute(client, message, [user1, user2]) {
    // These determine if you entered in the 2 arguments, member 1 and member 2
    if (!user1) return message.reply("Uh? You have to mention the first person, duh!")
    if (!user2) return message.reply("You forgot the second person! You silly goose!")

    const num = Math.floor(Math.random() * 101); // This is the number generator

    // Define the embed
    const embed = new Discord.MessageEmbed()
      .setTitle("Ship")
      .setColor(0xe31b23)
      .addField("Person 1", user1)
      .addField("Person 2", user2)
      .addField("Ship Rating", `:heart: ${num}%`)
      .setTimestamp()
      .setFooter("Shitpost");

    // Return the embed
    return message.channel.send(embed);
  }
};