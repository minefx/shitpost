const Discord = require("discord.js");
const ver = require("../package.json").version;

module.exports = {
  name: "info",
  description: "Bot info",
  execute(client, message, args) {
    // Define the embed
    const embed = new Discord.MessageEmbed()
      .setTitle("Info")
      .setDescription("The shittiest bot known to Discord.")
      .addField("Creator", "MineFX")
      .addField("Engine", "Discord.js")
      .addField("Bot Version", ver)
      .addField("Host", "AdminRAT's Server")
      .addField("Developers", "MineFX | AdminRAT")
      .setThumbnail(client.user.displayAvatarURL)
      .setColor(0x00ACEE)
      .setTimestamp()
      .setFooter("Shitpost");

    // Return the embed
    return message.channel.send(embed);
  }
};