const Discord = require("discord.js")
const sql = require("../libs/sqlite.js");

module.exports = {
  name: "balance",
  description: "Member cash balance",
  async execute(client, message, args) {
    // Get the user's cash row
    const row = sql.prepare("SELECT * FROM cash WHERE user = ? AND guild = ?").get(message.author.id, message.guild.id); // Grab the current user's cash

    if (!row) { // If there's no row, return
      return message.reply("I can't find you in my database! Run s!payday to get your first $100!");
    } else { // ...otherwise return the user's cash amount
      const embed = new Discord.MessageEmbed()
        .setTitle("💰 Balance")
        .setColor(0x00ff00)
        .addField("User", message.author)
        .addField("Balance", `$${row.cash}`);

      return message.channel.send(embed);
    }
  }
}