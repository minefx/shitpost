function rereact(message) {
  message.react("💰");
  message.react("🎉");
  message.react("⚙️");
  message.react("🤖");
}

const Discord = require("discord.js");

const helpHome = new Discord.MessageEmbed()
  .setTitle("Help")
  .setColor(0x00a6ff)
  .setDescription("Select an emoji below to change to that category!")
  .addField("💰", "Money Commands")
  .addField("🎉", "Fun Commands")
  .addField("⚙️", "Bot Settings")
  .addField("🤖", "Misc Bot Info")

const helpMoney = new Discord.MessageEmbed()
  .setTitle("Help: Money")
  .setColor(0x00ff6e)
  .addField("payday", "Get a couple bucks!")
  .addField("balance", "See your balance!")
  .addField("leaderboard", "See the people who have the most money in your guild!")
  .addField("rob", "Feeling nasty? Try and rob someone for all they have!")
  .addField("donate", "Feeling generous? Give someone some of your money!");
  

const helpFun = new Discord.MessageEmbed()
  .setTitle("Help: Fun")
  .setColor(0xffca45)
  .addField("calculate", "Basic four function calculator.")
  .addField("f", "Press f to pay respects!")
  .addField("gayrate", "See how gay someone is!")
  .addField("8ball", "Answer all life's secrets with the magic 8 ball!")
  .addField("ask", "Ask shitpost a yes or no question")
  .addField("avatar", "View someone's avatar.")
  .addField("ihadastroke", "Get a random post from r/ihadastroke.")
  .addField("joke", "Shitpost tells you a dad joke.")
  .addField("meme", "Get a random post from r/memes.")
  .addField("roll", "Roll a dice!")
  .addField("ship", "Ship 2 people or objects!")
  .addField("softwaregore", "Get a random post from r/softwaregore.")
  .addField("whoosh", "Get a random post from r/whoosh.");

const helpSettings = new Discord.MessageEmbed()
  .setTitle("Help: Settings")
  .setColor(0xb8b6b2)
  .addField("Settings", "Change bot settings")
  .addField("SetMoney", "[Admin Role only!] Set the money of a member to a specific value")
  .addField("GiveMoney", "[Admin Role only!] Add money to a member's existing balance")

const helpBot = new Discord.MessageEmbed()
  .setTitle("Help: Bot")
  .setColor(0xf5f5f5)
  .addField("ping", "View shitpost's ping.")
  .addField("info", "View bot info.")
  .addField("invite", "Get a link to invite shitpost to your server!")
  .addField("changelog", "View changes in the latest update.")
  .addField("License", "[Information for nerds] View bot src code licesne.");

module.exports = {
  name: "help",
  description: "Shows all commands",
  async execute(client, message, args) {
    message.channel.send(helpHome).then(message => {
      message.react("💰");
      message.react("🎉");
      message.react("⚙️");
      message.react("🤖");

      const emojis = [
        "💰",
        "🎉",
        "⚙️",
        "🤖",
      ];

      const filter = (reaction, user) => emojis.includes(reaction.emoji.name) && user.id !== message.author.id;
      const collector = message.createReactionCollector(filter, { time : 60000 });

      // TODO Remove only the user's mention when reacting
      collector.on("collect", async reaction => {
        if (reaction.emoji.name === "💰") {
          message.edit(helpMoney);
          await message.reactions.removeAll()
          rereact(message)
        } else
        if (reaction.emoji.name === "🎉") {
          message.edit(helpFun);
          await message.reactions.removeAll()
          rereact(message)
        }
        if (reaction.emoji.name === "⚙️") {
          message.edit(helpSettings);
          await message.reactions.removeAll()
          rereact(message)
        }
        if (reaction.emoji.name === "🤖") {
          message.edit(helpBot);
          await message.reactions.removeAll()
          rereact(message);
        }
      });
    })
  }
};