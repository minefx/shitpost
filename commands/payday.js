// Credit goes to AdminRAT for writing this.

const sql = require("../libs/sqlite.js");
const moment = require("moment");

module.exports = {
  name: "payday",
  description: "Give people free money because yes.",
  execute(client, message, args, settings) {
    // Define the row for the user
    const row = sql.prepare("SELECT * FROM cash WHERE user = ? AND guild = ?").get(message.author.id, message.guild.id);

    //Define paydayTime
    const paydayTime = settings.paydayTime
    // If there's no row, create a row
    if (!row) {
      const newAccount = sql.prepare("INSERT INTO cash (user, cash, guild, next_payday, next_rob) VALUES (?, ?, ?, ?, ?)");
      const time = parseInt(moment().add(paydayTime, "seconds").unix());
      newAccount.run(message.author.id, 100, message.guild.id, time, 0);

      return message.channel.send(`You have collected $100 ${message.author.tag}`);
    } else { // Otherwise add to their account
      // If they're running payday too quick, return and don't give them anything
      // TODO Add remaining time until they can run the command again
      if (row.next_payday > moment().unix()) return message.channel.send("Too fast there, wait a while before claiming your next payday");

      const newCashAmount = row.cash + 100;
      const time = parseInt(moment().add(paydayTime, "seconds").unix());
      sql.prepare("UPDATE cash SET cash = ?, next_payday = ? WHERE user = ? AND guild = ?").run(newCashAmount, time, message.author.id, message.guild.id);
      return message.channel.send(`You have collected $100 ${message.author.tag}`);
    };
  }
}