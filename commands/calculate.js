module.exports = {
  name: "calculate",
  description: "Basic 4 function calculator",
  execute(client, message, [firstNumber, operator, secondNumber]) {
    // If there's no argument defined, return
    if (!firstNumber) return message.channel.send(":x: Please list your first value!");
    if (!operator) return message.channel.send(":x: Please list your operator!");
    if (!secondNumber) return message.channel.send(":x: Please list your second value!");

    // Pick an operator and return the result
    switch (operator) {
      case "+":
        message.channel.send(parseInt(firstNumber) + parseInt(secondNumber));
        break;

      case "-":
        message.channel.send(parseInt(firstNumber) - parseInt(secondNumber));
        break;

      case "*":
      case "x":
        message.channel.send(parseInt(firstNumber) * parseInt(secondNumber));
        break;

      case "/":
        message.channel.send(parseInt(firstNumber) / parseInt(secondNumber));
        break;

      default:
        message.channel.send(":x: That is not a valid operator.");
        break;
    }
  }
};