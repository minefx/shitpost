const Discord = require("discord.js");

module.exports = {
  name: "invite",
  description: "Invite shitpost to your server",
  execute(client, message, args) {
    // Define the embed
    const embed = new Discord.MessageEmbed()
      .setTitle("Invite")
      .setDescription("So you'd like to invite me to your server?")
      .addField("Link", "https://discord.com/api/oauth2/authorize?client_id=688459408107110425&permissions=3072&scope=bot")
      .setColor(0x42ff7b)
      .setTimestamp()
      .setFooter("Shitpost"); // REVIEW Do you want the bot icon here?

    // Return the embed
    return message.channel.send(embed);
  }
};