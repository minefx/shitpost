module.exports = {
  name: "ping",
  description: "it like pings and then it pongs but it's not ping pong",
  async execute(client, message, args) {
    const msg = await message.channel.send("Ping?");
    return msg.edit(`Pong! Latency is ${msg.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ws.ping)}ms`);
  }
};