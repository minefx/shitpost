const Discord = require("discord.js");

module.exports = {
  name: "avatar",
  description: "Display a member's avatar.",
  execute(client, message, args) {
    // Define the user, otherwise fallback to the author
    const user = message.mentions.users.first() || message.author;

    // Get the avatar URL
    const avatarURL = user.displayAvatarURL({ size: 1024 });

    // Define the embed
    const embed = new Discord.MessageEmbed()
      .setAuthor(user.username, user.displayAvatarURL())
      .setColor(0x00ACEE)
      .setImage(avatarURL)
      .setTimestamp()
      .setFooter("Shitpost");

      // Return the embed
    return message.channel.send(embed);
  }
};