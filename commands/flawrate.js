module.exports = {
  name: "flawrate",
  description: "See how flawed you are",
  execute(client, message, args) {
    // If there's no argument, return
    if (!args) return message.reply("Please define something for me to rate!");

    // Join the args together and pick a random rating percentage
    const user = args.join(" ");
    const rating = Math.floor(Math.random() * Math.floor(100));

    // Return the user and rating
    // REVIEW Is user supposed to be the argument or mentioning a user?
    return message.channel.send(`${user} is ${rating}% flawed!`);
  }
}