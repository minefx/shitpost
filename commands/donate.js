const sql = require("../libs/sqlite.js");
module.exports = {
  name: "donate",
  description: "Give people money :D",
  execute(client, message, args) {
    // Parse the arguments
    const member = message.mentions.users.first();
    let cashAmount = args[1];
    // If there's no argument specified, return
    if (!member) return message.reply("Please mention a member to donate to!");
    if (!cashAmount) return message.reply("Please defined how much money you want to donate!");

    cashAmount = parseInt(cashAmount); // Try to parse cashAmount as an integer

    // Get the author and user's cash
    const authorCash = sql.prepare("SELECT * FROM cash WHERE user = ? AND guild = ?").get(message.author.id, message.guild.id);
    const userCash = sql.prepare("SELECT * FROM cash WHERE user = ? AND guild = ?").get(member.id, message.guild.id);

    if (!userCash) return message.reply("The member you are trying to donate to does not have a bank account, tell them to run s!payday!")
    if (!authorCash) return message.reply("You do not have a bank account, run s!payday to get some money!");
    // If the author's cash is more than cashAmount, return
    if (authorCash.cash < cashAmount) return message.reply("You do not have that much money!");

    // Give the specified cash amount to the user
    sql.prepare("UPDATE cash SET cash = ? WHERE user = ? AND guild = ?").run(authorCash.cash - cashAmount, message.author.id, message.guild.id);
    sql.prepare("UPDATE cash SET cash = ? WHERE user = ? AND guild = ?").run(userCash.cash + cashAmount, member.id, message.guild.id);

    // Let the user know that the money has been transferred
    return message.reply("You just gave " + member.tag + " $" + cashAmount + "!");
  }
}