const Discord = require("discord.js");
const request = require("request");

module.exports = {
  name: "ihadastroke",
  description: "Get a random post from r/ihadastroke",
  execute(client, message, args) {
    message.channel.startTyping();

    request("https://www.reddit.com/r/ihadastroke/random/.json", { json: true }, function(err, res, body) {
      if (err) {
        message.channel.stopTyping();
        console.error(err);
        return message.channel.send("Something went wrong while fetching data from Reddit.");
      }

      // Define the post data and extra stuff
      const post = body[0].data.children[0].data;
      const url = `https://reddit.com${post.permalink}`;
      const image = post.url;
      const title = post.title;
      const upvotes = post.ups;
      const downvotes = post.downs;
      const comments = post.num_comments;

      // Define the embed
      const embed = new Discord.MessageEmbed()
        .setColor(0xFF5700)
        .setImage(image)
        .addField(title, `[View Thread](${url})`)
        .setFooter(`\u{1F44D} ${upvotes} \u{1F44E} ${downvotes} \u{1F5E8} ${comments}`);

      // REVIEW We don't need this (debug) line anymore do we?
      // console.log(`Sent a reply to ${message.author.username}`);

      // Stop typing and return the embed
      message.channel.stopTyping();
      return message.channel.send(embed);
    });
  }
};
