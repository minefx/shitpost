const sql = require("../libs/sqlite.js");
const moment = require("moment");
module.exports = {
  name: "rob",
  description: "Rob someone of their money",
  execute(client, message, args, settings) {
    const member = message.mentions.users.first();
    if (!member) return message.reply("Who you gonna rob, mate?");

    // Fetch the author and user's cash
    const authorCash = sql.prepare("SELECT * FROM cash WHERE user = ? AND guild = ?").get(message.author.id, message.guild.id);
    const userCash = sql.prepare("SELECT * FROM cash WHERE user = ? AND guild = ?").get(member.id, message.guild.id);
    if (!authorCash) return message.reply("You have not run payday! Run the payday command to get some money!");
    if (!userCash) return message.reply("That user does not exist in my database! Tell them to run the payday command to get some money!");
    // Pick a random chance between true and false
    const chance = Math.floor(Math.random() * 2) ? true : false;

    // If true, the author has successfully robbed the user

    // Check if you can rob the player.
    const robTime = settings.robTime;
    if (authorCash.next_rob > moment().unix()) return message.channel.send("Too fast there, wait a while before robbing someone again!");

    const time = parseInt(moment().add(robTime, "seconds").unix());
    if (chance) {
      const robAmount = Math.round(userCash.cash / 2);

      sql.prepare("UPDATE cash SET cash = ? WHERE user = ? AND guild = ?").run(userCash.cash - robAmount, member.id, message.guild.id);
      sql.prepare("UPDATE cash SET cash = ?, next_rob = ? WHERE user = ? AND guild = ?").run(robAmount + authorCash.cash, time, message.author.id, message.guild.id);

      return message.channel.send("💰 You successfully robbed " + member.tag + " of $" + robAmount);
    } else { // ...otherwise, it's the opposite instead
      const lostAmount = Math.round(authorCash.cash / 2);

      sql.prepare("UPDATE cash SET cash = ? WHERE user = ? AND guild = ?").run(authorCash.cash - lostAmount, message.author.id, message.guild.id);
      sql.prepare("UPDATE cash SET cash = ?, next_rob = ? WHERE user = ? AND guild = ?").run(userCash.cash + lostAmount, time, member.id, message.guild.id);

      return message.channel.send("You got caught! The person you were trying to rob pressed charges and you lost half your money! Shame on you!");
    }
  }
};