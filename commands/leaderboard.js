const Discord = require("discord.js");
const sql = require("../libs/sqlite.js");
const prefix = process.env.PREFIX;

module.exports = {
  name: "leaderboard",
  description: "Cash leaderboard.",
  async execute(client, message, args) {
    // Get the guild's users and cash
    // REVIEW Probably limit the number of rows returned. Would 5 be good?
    const row = sql.prepare("SELECT * FROM cash WHERE guild = ? ORDER BY cash DESC LIMIT 5").all(message.guild.id);

    // If there are no rows, return
    if (row.length === 0) return message.reply(`:x: No one has ran \`${prefix}payday\`, yet! Run \`${prefix}payday\` to be the first person on the leaderboard!`); // REVIEW Rephrase the first part since you've mentioned the command 2 times?
    else { // ...otherwise, show a leaderboard
      // Define the embed
      const embed = new Discord.MessageEmbed()
        .setTitle("Leaderboard")
        .setThumbnail(message.guild.iconURL())
        .setColor("#00ff00")
        .setTimestamp()
        .setFooter("Shitpost")

      // Credit to AdminRAT for the loop.
      // Loop each user that's in the database
      for (i = 0; i < row.length; i++) {
        const user = await message.guild.members.fetch(row[i].user); // Fetch the user
        await embed.addField(user.user.tag, row[i].cash.toLocaleString()); // Add the field to the embed
      };

      // Return the embed
      return message.channel.send(embed);
    }
  }
}
