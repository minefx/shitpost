const responses = [
  "As I see it, yes.",
  "Ask again later.",
  "Better not tell you now.",
  "Cannot predict now.",
  "Concentrate and ask again.",
  "Don’t count on it.",
  "It is certain.",
  "It is decidedly so.",
  "Most likely.",
  "My reply is no.",
  "My sources say no.",
  "Outlook not so good.",
  "Outlook good.",
  "Reply hazy, try again.",
  "Signs point to yes.",
  "Very doubtful.",
  "Without a doubt.",
  "Yes.",
  "Yes – definitely.",
  "You may rely on it."
];

module.exports = {
  name: "8ball",
  description: "See all life's secrets",
  execute(client, message, args) {
    if (!args.join(" ")) return message.reply("What do you want to ask the majestic 8ball?")
    return message.channel.send(responses[Math.floor(Math.random() * responses.length)]); // Return a random response
  }
};
