const Discord = require("discord.js");
const request = require("request");

module.exports = {
  name: "softwaregore",
  description: "Pull a random post from r/softwaregore",
  execute(client, message, args) {
    message.channel.startTyping();

    request("https://www.reddit.com/r/softwaregore/random/.json", { json: true }, function (err, res, body) {
      if (err) {
        message.channel.stopTyping();
        console.error(err);
        return message.channel.send("Something went wrong while fetching data from Reddit.");
      }

      // Define the post data and other stuff
      const post = body[0].data.children[0].data;

      const url = `https://reddit.com${post.permalink}`;
      const image = post.url;
      const title = post.title;
      const upvotes = post.ups;
      const downvotes = post.downs;
      const comments = post.num_comments;

      // Define the embed
      const embed = new Discord.MessageEmbed()
        .setColor(0xFF5700)
        .setImage(image)
        .addField(title, `[View Thread](${url})`)
        .setFooter(`\u{1F44D} ${upvotes} \u{1F44E} ${downvotes} \u{1F5E8} ${comments}`);

      // REVIEW Still need this (debug) line?
      // console.log(`Sent a reply to ${message.author.username}`);

      // Stop typing and return the embed
      message.channel.stopTyping();
      return message.channel.send(embed);
    });
  }
};
