module.exports = {
  name: "ask",
  description:"Ask shitpost a yes or no question",
  execute(client, message, args) {
    // If there's no question, say something
    if (!args[0]) return message.reply("Well, what are you waiting for? Ask the question already dammit!");

    // Pick an answer and say yes or no
    const answer = Math.floor(Math.random() * 2) ? true : false;
    if (answer) return message.channel.send("Yes");
    else return message.channel.send("No");
  }
};