module.exports = {
  name: "roll",
  description: "Roll a dice",
  execute(client, message, args) {
    return message.channel.send(`You rolled a ${Math.floor(Math.random() * 6) + 1}!`); // Roll a number between 1 and 6
  }
};