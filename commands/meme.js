const Discord = require("discord.js");
const request = require("request");

module.exports = {
  name: "meme",
  description: "Pull a random meme from r/memes",
  execute(client, message, args) {
    message.channel.startTyping();

    request("https://www.reddit.com/r/memes/random/.json", { json: true }, function (err, res, body) {
      if (err) {
        message.channel.stopTyping();
        console.error(err);
        return message.channel.send("Something went wrong while fetching data from Reddit.");
      }

      // Define the data and extra stuff
      const meme = body[0].data.children[0].data;

      const url = `https://reddit.com${meme.permalink}`;
      const image = meme.url;
      const title = meme.title;
      const upvotes = meme.ups;
      const downvotes = meme.downs;
      const comments = meme.num_comments;

      // Define the embed
      const embed = new Discord.MessageEmbed()
        .setColor(0xFF5700)
        .setImage(image)
        .addField(title, `[View Thread](${url})`)
        .setFooter(`\u{1F44D} ${upvotes} \u{1F44E} ${downvotes} \u{1F5E8} ${comments}`);

      // REVIEW Do we still need this (debug) line?
      // console.log(`Sent a reply to ${message.author.username}`);

      // Stop typing and return the embed
      message.channel.stopTyping();
      return message.channel.send(embed);
    });
  }
};