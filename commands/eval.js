module.exports = {
  name: "eval",
  description: "Execute code client side.",
  async execute(client, message, args) {
    // If the user is not the owner, return.
    if (message.author.id !== process.env.OWNER_ID) return message.reply("Only the bot owner can do that!");

    // Eval the code
    const code = args.join(" ");
    try {
      const evaled = eval(code)
      message.channel.send(`\`\`\`js\n${evaled}\n\`\`\``);
    } catch (err) {
      message.channel.send(`\`ERROR\` \`\`\`xl\n${await client.clean(client, err)}\n\`\`\``);
    }
  }
}