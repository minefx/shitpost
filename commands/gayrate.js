module.exports = {
  name: "gayrate",
  description: "See how gay someone is!",
  execute(client, message, args) {
    // Pick a random percentage
    const percentage = Math.floor(Math.random() * 101);

    // REVIEW Maybe have this for the command since you're mentioning the member?
    //const gay = message.mentions.members.first();
    const yes = args.join(" ");

    // If there's no arguments, return the author's gay percentage
    // REVIEW Mention the author or just say their username/tag?
    if (!yes) return message.channel.send(`${message.author.username} is ${percentage}% gay!`);
    else return message.channel.send(`${yes} is ${percentage}% gay!`);
  }
};