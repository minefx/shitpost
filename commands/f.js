module.exports = {
  name: "f",
  description: "Press f to pay respects",
  execute(client, message, args) {
    // If there's no args, say that the author has paid their respects.
    const fReason = args.join(" ");
    if (!fReason) return message.channel.send(`<@${message.author.id}> has pressed f to pay their respects.`);

    // Join the args together and return the author's respect for (fReason).
    return message.channel.send(`<@${message.author.id}> has paid their respects for ${fReason}`);
  }
};
