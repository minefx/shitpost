const Discord = require("discord.js");
const sql = require("../libs/sqlite.js");

module.exports = {
    name: "settings",
    description: "Change bot settings",
    async execute(client, message, [key, setting]) {
        const row = sql.prepare("SELECT * FROM botSettings WHERE guild = ?").get(message.guild.id);
        if (!row) {
            await sql.prepare("INSERT INTO botsettings (guild, prefix, cashModRole, botAdminRole, paydayTime, robTime) VALUES (?, ?, ?, ?, ?, ?)")
                .run(message.guild.id, "s!", "Administrator", "Administrator", 60, 60);
            return message.reply("I couldn't find the settings database for your server, so I just created one for you. Go ahead and rerun the command!");
        } else {
            if (message.member.roles.cache.has(row.cashModRole) || message.member.hasPermission("ADMINISTRATOR")) {
                const usage = new Discord.MessageEmbed()
                    .setTitle("Usage:")
                    .setColor(0x0a528a)
                    .setDescription('s!settings \`[Key]\` \`[Changes]\`')
                    .setTimestamp()
                    .setFooter("Shitpost");
                const servSets = new Discord.MessageEmbed()
                    .setTitle("Server settings: ")
                    .setThumbnail(message.guild.iconURL())
                    .setColor(0x00ff9d)
                    .setDescription("Keys: \`Prefix\` \`CashMGR\` \`AdminRole\`  \`PaydayTime\` \`RobTime\` (Not case sensitive)")
                    .addField("Prefix", `\`${row.prefix}\``)
                    .addField("Cash Manager Role", `\`${row.cashModRole}\``)
                    .addField("Bot Admin Role", `\`${row.botAdminRole}\``)
                    .addField("Payday Time", `\`${row.paydayTime}\``)
                    .addField("Rob Time", `\`${row.robTime}\``)
                    .setTimestamp()
                    .setFooter("Shitpost");

                if (!key) {
                    await message.channel.send(servSets);
                    return message.channel.send(usage);
                }
                if (!row) {
                    const createSettings = sql.prepare("INSERT INTO botSettings (guild, prefix, cashModRole, botAdminRole) VALUES (?, ?, ?, ?)");
                    createSettings.run(message.guild.id, "s!", "Administrator", "Administrator");
                }

                if (key.toLowerCase() === "prefix") {
                    if (!setting) return message.channel.send("Define \`setting\` as text.")
                    sql.prepare("UPDATE botSettings SET prefix = ? WHERE guild = ?").run(setting, message.guild.id);
                    message.reply(`:white_check_mark: Successfully changed prefix to \`${setting}\`!`);
                }
                else if (key.toLowerCase() === "cashmgr") {
                    if (!setting) return message.channel.send("Define \`setting\` as a mention. (Mention a role.)");
                    const mgrRole = setting.slice(3, -1);
                    sql.prepare("UPDATE botSettings SET cashModRole = ? WHERE guild = ?").run(mgrRole, message.guild.id);
                    message.reply(`:white_check_mark: Succesfully changed the Cash Manager role to \`${mgrRole}\`!`);
                }
                else if (key.toLowerCase() === "adminrole") {
                    if (!setting) return message.channel.send("Define \`setting\` as a mention. (Mention a role.)");
                    const adminRole = setting.slice(3, -1);
                    sql.prepare("UPDATE botSettings SET botAdminRole = ? WHERE guild = ?").run(adminRole, message.guild.id);
                    message.reply(`:white_check_mark: Successfully changed Admin Role to \`${adminRole}\`!`)
                }
                else if (key.toLowerCase() === "paydaytime") {
                    if (!setting) return message.channel.send("Define \`setting\` as a number (in seconds).");
                    sql.prepare("UPDATE botSettings SET paydayTime = ? WHERE guild = ?").run(setting, message.guild.id);
                    message.reply(`:white_check_mark: Successfully changed Payday Time to \`${setting}\`!`)
                }
                else if (key.toLowerCase() === "robtime") {
                    if (!setting) return message.channel.send("Define \`setting\` as a number (in seconds).");
                    sql.prepare("UPDATE botSettings set robTime = ? WHERE guild = ?").run(setting, message.guild.id);
                    message.reply(`:white_check_mark: Successfully changed Rob Time to \`${setting}\`!`)
                }
                else {
                    await message.reply("I can't find the key you've defined anywhere in my databases!");
                    message.channel.send(usage);
                }
            } else return message.reply("You do not have permission to run this command! Shoo! Shoo!");
        }
    },
}