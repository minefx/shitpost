module.exports = {
  name: "reboot",
  description: "reboot bot",
  async execute(client, message, args) {
    // If the user is not the owner, return
    if (message.author.id !== process.env.OWNER_ID) return message.reply("Only the bot owner can do that!")

    // Otherwise stop the bot
    await message.reply("Bot is shutting down.");
    await client.destroy(); // Destory the client tp prevent a "ghost bot"
    return process.exit(26);
  }
};