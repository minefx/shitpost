const Discord = require("discord.js");
const version = require("../package.json").version;
const changelog = require("../changelog.json").versions

const ver = changelog.find(c => c.version === version); // Find the version in the changelog that matches the verison in package.json

module.exports = {
  name: "changelog",
  description: "Pull changelog from changelog.json",
  execute(client, message, args) {
    const embed = new Discord.MessageEmbed()
      .setTitle("Changelog")
      .setColor(0xf5f5f5)
      .addField("Version", version)
      .addField("Changes", ver.changes);

    return message.channel.send(embed);
  }
}